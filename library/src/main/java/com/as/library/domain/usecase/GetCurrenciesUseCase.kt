package com.`as`.library.domain.usecase

import com.`as`.library.domain.repository.CurrencyRepository

class GetCurrenciesUseCase (private val currencyRepository: CurrencyRepository) {
    suspend operator fun invoke() = currencyRepository.getCurrencies()
}