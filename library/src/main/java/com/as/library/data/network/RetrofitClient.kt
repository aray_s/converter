package com.`as`.library.data.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClient {

    private const val BASE_URL = "https://api.apilayer.com"
    private const val API_KEY = "nxyza5wIkx5OEzA23odhUJkolvZYhNjP"
    private const val timeout = 60L
    private val okHttpClient = OkHttpClient.Builder()
        .connectTimeout(timeout, TimeUnit.SECONDS)
        .readTimeout(timeout, TimeUnit.SECONDS)
        .addInterceptor(AuthInterceptor(API_KEY))
        .build()

    private val instance: Retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    val retrofitServices: RetrofitServices = instance.create(RetrofitServices::class.java)
}
