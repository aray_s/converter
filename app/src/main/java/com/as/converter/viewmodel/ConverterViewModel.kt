package com.`as`.converter.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.`as`.library.data.mapper.CurrencyMapperImpl
import com.`as`.library.data.repository.CurrencyRepositoryImpl
import com.`as`.library.data.storage.CurrencyStorageImpl
import com.`as`.library.domain.usecase.ConvertMoneyUseCase
import com.`as`.library.domain.usecase.GetCurrencyNamesUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ConverterViewModel(
    private val GetCurrencyNamesUseCase: GetCurrencyNamesUseCase = defaultGetCurrencyNamesUseCase,
    private val ConvertMoneyUseCase: ConvertMoneyUseCase = defaultConvertMoneyUseCase,
) : ViewModel() {

    val symbols = MutableLiveData<List<String>>()

    val result = MutableLiveData<Double>()

    fun getSymbols() =
        viewModelScope.launch(Dispatchers.IO) { symbols.postValue(GetCurrencyNamesUseCase.invoke()) }

    fun convertCurrencies(from: String, to: String, amount: Double) =
        viewModelScope.launch(Dispatchers.IO) {
            result.postValue(
                ConvertMoneyUseCase.invoke(
                    from,
                    to,
                    amount
                )
            )
        }


    companion object {
        private val defaultRepository =
            CurrencyRepositoryImpl(CurrencyStorageImpl(CurrencyMapperImpl()))
        private val defaultGetCurrencyNamesUseCase = GetCurrencyNamesUseCase(defaultRepository)
        private val defaultConvertMoneyUseCase = ConvertMoneyUseCase(defaultRepository)
    }
}
