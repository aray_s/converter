package com.`as`.library.data.model

data class Fluctuation(
    val rates: Map<String, CurrencyRateChangeInfo>,
)