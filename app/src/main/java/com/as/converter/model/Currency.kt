package com.`as`.converter.model

data class Currency (
    val name: String,
    val rate: Double,
    val changes: Double
)