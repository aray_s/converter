package com.`as`.library.data.model

data class Symbols(
    val symbols: Map<String, String>,
)