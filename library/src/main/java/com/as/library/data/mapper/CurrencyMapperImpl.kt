package com.`as`.library.data.mapper

import com.`as`.library.data.model.CurrencyRateChangeInfo
import com.`as`.library.domain.model.Currency

class CurrencyMapperImpl : CurrencyMapper {
    override fun mapper(entry: Map.Entry<String, CurrencyRateChangeInfo>) = Currency(
        name = entry.key,
        rate = entry.value.endRate,
        changes = entry.value.change,
    )
}
