package com.`as`.library.data.mapper

import com.`as`.library.data.model.CurrencyRateChangeInfo
import com.`as`.library.domain.model.Currency

interface CurrencyMapper {
    fun mapper(entry: Map.Entry<String, CurrencyRateChangeInfo>): Currency
}
