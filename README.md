This app consists of two modules: library and app.

- Clean Architecture, MVVM
- Coroutines
- Retrofit
- Espresso


![image](/uploads/f335f99c48b2abd1ec913b53fc868301/image.png){width=200}

![image](/uploads/0f8647d9f4d92f15516ea8adf217b212/image.png){width=200}

![image](/uploads/b3ddba508bd6c35f6f12ed5feb01667a/image.png){width=200}
