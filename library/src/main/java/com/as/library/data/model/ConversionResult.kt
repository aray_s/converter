package com.`as`.library.data.model

data class ConversionResult(
    val result: Double,
)