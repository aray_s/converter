package com.`as`.library.data.repository

import com.`as`.library.data.storage.CurrencyStorage
import com.`as`.library.domain.model.Currency
import com.`as`.library.domain.repository.CurrencyRepository

class CurrencyRepositoryImpl (private val dataStorage: CurrencyStorage) : CurrencyRepository {

    override suspend fun getCurrencyNames(): List<String> = dataStorage.getCurrencyNames()

    override suspend fun getCurrencies(): List<Currency> = dataStorage.getCurrencies()

    override suspend fun convertMoney(convertFrom: String, convertTo: String, amount: Double): Double =
        dataStorage.convertMoney(convertFrom, convertTo, amount)
}