package com.`as`.library.data.model

import com.google.gson.annotations.SerializedName

data class CurrencyRateChangeInfo (
    val change: Double,
    @SerializedName("end_rate") val endRate: Double,
)