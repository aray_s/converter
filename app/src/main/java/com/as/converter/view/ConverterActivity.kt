package com.`as`.converter.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import com.`as`.converter.databinding.ActivityConverterBinding
import com.`as`.converter.viewmodel.ConverterViewModel

class ConverterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityConverterBinding
    private val viewModel: ConverterViewModel by viewModels()
    private lateinit var from: String
    private lateinit var to: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConverterBinding.inflate(layoutInflater)
        setContentView(binding.root)


        viewModel.result.observe(this) { binding.textView.text = "Result: $it" }

        viewModel.symbols.observe(this) {
            val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, it)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.fromSpinner.adapter = adapter
            binding.toSpinner.adapter = adapter
        }

        setOnClickListeners()

        viewModel.getSymbols()
    }


    private fun setOnClickListeners() {
        binding.calculate.setOnClickListener {
            viewModel.convertCurrencies(from, to , binding.editText.text.toString().toDouble())
        }

        binding.fromSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                from = parent.getItemAtPosition(pos) as String
            }
            override fun onNothingSelected(parent: AdapterView<*>) {
                from = parent.getItemAtPosition(0) as String
            }
        }

        binding.toSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                to = parent.getItemAtPosition(pos) as String
            }
            override fun onNothingSelected(parent: AdapterView<*>) {
                to = parent.getItemAtPosition(0) as String
            }
        }
    }
}