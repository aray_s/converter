package com.`as`.library.domain.model

data class Currency(
    val name: String,
    val rate: Double,
    val changes: Double
)