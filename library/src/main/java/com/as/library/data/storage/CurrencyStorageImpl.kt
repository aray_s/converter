package com.`as`.library.data.storage

import com.`as`.library.data.mapper.CurrencyMapper
import com.`as`.library.data.network.RetrofitClient
import com.`as`.library.domain.model.Currency
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class CurrencyStorageImpl(private val mapper: CurrencyMapper) : CurrencyStorage {

    override suspend fun getCurrencyNames(): List<String> =
        RetrofitClient.retrofitServices.getSymbols().symbols.keys.toList()

    override suspend fun getCurrencies(): List<Currency> {
        val currentDate = LocalDate.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        return RetrofitClient.retrofitServices.fluctuation(
            currentDate.format(formatter),
            currentDate.minusDays(1).format(formatter),
            BASE
        ).rates.entries.map(mapper::mapper)
    }

    override suspend fun convertMoney(from: String, to: String, amount: Double): Double =
        RetrofitClient.retrofitServices.convertMoney(from, to, amount).result

    companion object {
        private const val BASE = "EUR"
    }
}