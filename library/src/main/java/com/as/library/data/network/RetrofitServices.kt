package com.`as`.library.data.network

import com.`as`.library.data.model.ConversionResult
import com.`as`.library.data.model.Fluctuation
import com.`as`.library.data.model.Symbols
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitServices {

    @GET("exchangerates_data/symbols")
    suspend fun getSymbols(): Symbols

    @GET("exchangerates_data/convert")
    suspend fun convertMoney(
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("amount") amount: Double
    ): ConversionResult

    @GET("/exchangerates_data/fluctuation")
    suspend fun fluctuation(
        @Query("start_date") startDate: String,
        @Query("end_date") endDate: String,
        @Query("base") base: String,
    ): Fluctuation
}