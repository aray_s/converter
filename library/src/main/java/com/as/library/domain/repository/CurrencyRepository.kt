package com.`as`.library.domain.repository

import com.`as`.library.domain.model.Currency

interface CurrencyRepository {
    suspend fun getCurrencies(): List<Currency>
    suspend fun getCurrencyNames(): List<String>
    suspend fun convertMoney(convertFrom: String, convertTo: String, amount: Double): Double
}