package com.`as`.library.domain.usecase

import com.`as`.library.domain.repository.CurrencyRepository

class ConvertMoneyUseCase(private val currencyRepository: CurrencyRepository) {
    suspend operator fun invoke(convertFrom: String, convertTo: String, amount: Double) =
        currencyRepository.convertMoney(convertFrom, convertTo, amount)
}