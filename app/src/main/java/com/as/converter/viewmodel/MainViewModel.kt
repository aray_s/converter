package com.`as`.converter.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.`as`.converter.model.Currency
import com.`as`.library.data.mapper.CurrencyMapperImpl
import com.`as`.library.data.repository.CurrencyRepositoryImpl
import com.`as`.library.data.storage.CurrencyStorageImpl
import com.`as`.library.domain.usecase.GetCurrenciesUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(
    private val getCurrenciesUseCase: GetCurrenciesUseCase = defaultGetCurrenciesUseCase
) : ViewModel() {

    val currencies = MutableLiveData<List<Currency>>()

    fun getCurrencies() = viewModelScope.launch(Dispatchers.IO) {
        currencies.postValue(getCurrenciesUseCase().map { it ->
            Currency(
                it.name,
                it.rate,
                it.changes
            )
        })
    }

    companion object {
        private val defaultRepository =
            CurrencyRepositoryImpl(CurrencyStorageImpl(CurrencyMapperImpl()))
        private val defaultGetCurrenciesUseCase = GetCurrenciesUseCase(defaultRepository)
    }
}