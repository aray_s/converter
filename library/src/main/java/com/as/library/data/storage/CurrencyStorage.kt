package com.`as`.library.data.storage

import com.`as`.library.domain.model.Currency

interface CurrencyStorage {
    suspend fun getCurrencyNames(): List<String>
    suspend fun getCurrencies(): List<Currency>
    suspend fun convertMoney(from: String, to: String, amount: Double): Double
}