package com.`as`.converter.view

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.`as`.converter.databinding.ActivityMainBinding
import com.`as`.converter.viewmodel.MainViewModel
import com.`as`.converter.view.adapter.CurrencyAdapter

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val adapter = CurrencyAdapter(this)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(this)

        binding.toConverterBtn.setOnClickListener { startActivity(Intent(this, ConverterActivity::class.java)) }

        viewModel.currencies.observe(this, Observer { list -> list?.let { adapter.updateList(it) } })
        viewModel.getCurrencies()
    }
}

