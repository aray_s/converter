package com.`as`.library.domain.usecase

import com.`as`.library.domain.repository.CurrencyRepository

class GetCurrencyNamesUseCase(private val currencyRepository: CurrencyRepository) {
    suspend operator fun invoke() = currencyRepository.getCurrencyNames()
}