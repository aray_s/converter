package com.`as`.converter.view.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.`as`.converter.databinding.ItemBinding
import com.`as`.converter.model.Currency

class CurrencyAdapter(private val context: Context) : RecyclerView.Adapter<CurrencyAdapter.ViewHolder>(){

    private lateinit var binding: ItemBinding
    private var currencyList = mutableListOf<Currency>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        binding = ItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currency = currencyList[position]
        holder.name.text = currency.name
        holder.changes.text = if (currency.changes>0) "+${currency.changes}" else "${currency.changes}"
        binding.root.setBackgroundColor(
            if (currency.changes>0) Color.GREEN
            else Color.RED
        )
    }

    fun updateList(newList: List<Currency>) {
        currencyList.clear()
        currencyList.addAll(newList)
        notifyDataSetChanged()
    }

    override fun getItemCount() = currencyList.size

    inner class ViewHolder(binding: ItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val name = binding.name
        val changes = binding.changes
    }
}
